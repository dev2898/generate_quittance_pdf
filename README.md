# Generate_Quittance_PDF

Un petit script python pour générer rapidement des quittance de loyer en PDF.

Après une configuration initiale avec vos informations personnelles (directement dans le fichier .py) Les mois suivants, il ne demande que :
- Le nom du locataire
- Le mois de la quittance
- L'année de la quittance

-> Et hop un fichier pdf à envoyer au locataire : [PDF_Screenshot](PDF_screenshot.png) 
