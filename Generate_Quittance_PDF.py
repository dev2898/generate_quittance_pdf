# Import FPDF class
from fpdf import FPDF

# Si le locataire est à jour de ses loyers alors les infos suivantes ne changent pas d'un mois à l'autre.
# Du coup pour gagner du temps, je ne les demande pas en input
# Si locataire pas à jour faire mise à jours manuelle avant exécution. Dans cette condition ça mérite de passer un peu de temps sur le dossier de son locataire :-/
ANTECEDENT = '0,00'
LOYER = '400'
CHARGE = '100'
TOTAL = '500'

ANTECEDENT_PAYEE = ' '
LOYER_PAYEE = '400'
CHARGE_PAYEE = '100'
TOTAL_PAYEE = '500'

# Le locataire_name est demandé en input.
# Ca permet de gérer plusieurs locataire à la même adresse (Colocation)
# L'adresse de location ne change pas (sauf si plusieurs bien loués. Alors modifier le code pour faire un input des valeurs ci-dessous)
LOCATAIRE_ADRESSE = '57 Rue de Varenne'
LOCATAIRE_CP = '75007'
LOCATAIRE_VILLE = 'PARIS'

# Le owner et ses coordonnées ne changent pas (en tout cas pas tous les mois).
PRORIO_NAME = 'M. Le Président de la République'
PROPRIO_ADRESSE = '55 Rue du Faubourg Saint-Honoré'
PROPRIO_CP = '75008'
PROPRIO_VILLE = 'PARIS'
PROPRIO_MAIL = 'contact@elysee.fr'
PROPRIO_TEL = '01.42.92.81.00'

# On demande le nom du locataire
print('Nom du locataire :')
locataire_name = input()

# On demande le numéro du mois de la quittance (de 01 à 12)
print('Quittance pour mois (exemple 01) :')
mois_number = input()

# On demande l'année de la quittance
print('Quittance pour année (exemple 2029) :')
annee = input()

# Ici je concatène le mois et l'année pour utilisation plus tard (exemple : 01-2029) 
edition_date = str(mois_number) + '-' + str(annee)

# A partir du numéro de mois renseigné dans l'input. Je créer deux nouvelles variables utilisable plus tard.
if mois_number == '01':
	mois_name = 'Janvier'
	mois_nbr_jours = '31'

elif mois_number == '02':
	mois_name = 'Février'
        # Faudrait prendre en compte les années bissextiles.
        # Mais beaucoup de calcul et condition a faire pour un truc qui arrive une fois tout les 4 ans.
        # soit corriger manuellement avant exécution du script en année bissextile.
        # Ou tant pis il y a une coquille tout les 4 ans. Le locataire devrait s'en remettre
	mois_nbr_jours = '28'

elif mois_number == '03':
        mois_name = 'Mars'
        mois_nbr_jours = '31'

elif mois_number == '04':
        mois_name = 'Avril'
        mois_nbr_jours = '30'

elif mois_number == '05':
        mois_name = 'Mai'
        mois_nbr_jours = '31'

elif mois_number == '06':
        mois_name = 'Juin'
        mois_nbr_jours = '30'

elif mois_number == '07':
        mois_name = 'Juillet'
        mois_nbr_jours = '31'

elif mois_number == '08':
        mois_name = 'Aout'
        mois_nbr_jours = '31'

elif mois_number == '09':
        mois_name = 'Septembre'
        mois_nbr_jours = '30'

elif mois_number == '10':
        mois_name = 'Octobre'
        mois_nbr_jours = '31'

elif mois_number == '11':
        mois_name = 'Novembre'
        mois_nbr_jours = '30'

elif mois_number == '12':
        mois_name = 'Décembre'
        mois_nbr_jours = '31'
else:
	print(mois_number +' n\'est pas un mois valide')
	quit()


# Create instance of FPDF class
# Je définis les caractéristiques de mon pdf (Portrait / A4 / centimètre)
pdf=FPDF(orientation = 'P', format='A4', unit='cm')

# J'ajoute une nouvelle page a mon pdf. Sans ça ne marche pas.
pdf.add_page()

# Toujours définir au moins une fois la police et la taille d'écriture.
pdf.set_font('Times','',10.0)

# Effective page width, or just epw (a clarifier)
epw = pdf.w - 2*pdf.l_margin

# Set column width to 1/4 of effective page width to distribute content
# evenly across table and page
col_width = epw/4

# Since we do not need to draw lines anymore, there is no need to separate
# headers from data matrix.
data = [['Date','Libellé','Appelé','Réglé'],
['05-' + edition_date,'Solde précédent',ANTECEDENT,ANTECEDENT_PAYEE],
['05-' + edition_date,'Prévision de charges ',CHARGE,CHARGE_PAYEE],
['05-' + edition_date,'Loyer mois de ' + mois_name + ' ' + annee ,LOYER,LOYER_PAYEE],
['05-' + edition_date,'Total',TOTAL,TOTAL_PAYEE]]

# Text height is the same as current font size
th = pdf.font_size

pdf.set_font('Times','',10.0)
# En haut à gauche lieu et date d'édition du document 
pdf.cell(epw, 0.0, PROPRIO_VILLE + ' le : 05-' + edition_date, align='R')
# On saute une ligne et en haut à droite les coordonnées du owner
pdf.ln(1*th)
pdf.cell(epw, 0.0, PRORIO_NAME, align='L')
pdf.ln(1*th)
pdf.cell(epw, 0.0, PROPRIO_ADRESSE, align='L')
pdf.ln(1*th)
pdf.cell(epw, 0.0, PROPRIO_CP + ' ' + PROPRIO_VILLE, align='L')
pdf.ln(1*th)
pdf.cell(epw, 0.0, PROPRIO_MAIL, align='L')
pdf.ln(1*th)
pdf.cell(epw, 0.0, PROPRIO_TEL, align='L')
# On saute 10 lignes et à gauche les coordonées du locataire
pdf.ln(10*th)
pdf.cell(epw, 0.0, locataire_name, align='R')
pdf.ln(1*th)
pdf.cell(epw, 0.0, LOCATAIRE_ADRESSE, align='R')
pdf.ln(1*th)
pdf.cell(epw, 0.0, LOCATAIRE_CP + ' ' + LOCATAIRE_VILLE, align='R')

# A ce stade on a une jolie lettre prête à être envoyée. Maintenant il faut lui donner du contenue.

# On saute 10 lignes
pdf.ln(10*th)

# "l'objet" de notre courrier. Gras, taille 14 et centré
pdf.set_font('Times','B',14.0)
pdf.cell(epw, 0.0, 'Quittance période du 01-' + edition_date + ' au ' + mois_nbr_jours + '-' + edition_date , align='C')
# On saute 5 lignes et on commence la rédaction de notre courrier. (taille 10 et aligné à gauche)
pdf.ln(5*th)
pdf.set_font('Times','',10.0)
pdf.cell(epw, 0.0, 'Madame, Monsieur', align='L')
pdf.ln(2*th)
pdf.cell(epw, 0.0, 'Je vous prie de trouver ci-dessous votre quittance de loyer pour la période citée en référence', align='L')

# On va maintenant ajouter notre tableau de facture / compta / quittance / autre... (Ce qui a été renseigné dans "data" plus haut)
pdf.set_font('Times','',10.0)
pdf.ln(5*th)

# Here we add more padding by passing 2*th as height
for row in data:
	for datum in row:
		# Enter data in colums
		pdf.cell(col_width, 2*th, str(datum), border=1)

	pdf.ln(2*th)

# Sous le tableau (taille 10 et centré) une petite annotation (Le symbole € fait planter fpdf. Il n'arrive pas a l'encoder)
pdf.ln(1*th)
pdf.set_font('Times','',10.0)
pdf.cell(epw, 0.0, 'Toute les sommes sont exprimées en Euros', align='C')

# Je sautre quelques lignes puis je continue mon courrier
pdf.ln(5*th)
pdf.set_font('Times','',10.0)
pdf.cell(epw, 0.0, 'Je vous en souhaite bonne réception, et vous prie d\'agréer, Madame, Monsieur, l\'expression de mes salutations distinguées', align='L')
pdf.ln(3*th)
# "Je signe" le courrier
pdf.set_font('Times','',10.0)
pdf.cell(epw, 0.0, PRORIO_NAME, align='L')

# Je vire les espaces dans locataire_name afin d'enregistrer le pdf avec un nom sans espace
locataire_name = locataire_name.replace(" ","")

# J'enregistre mon pdf sous la forme Quittance_YYYYMMJJ_LocataireName.pdf (d'où la suppression des espaces dans locataire_name)
pdf.output('Quittance_' + annee  + mois_number + '05_' + locataire_name +'_.pdf','F')
